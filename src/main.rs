use serde::{Serialize, Deserialize};
use serde::de::{self, Deserializer};
use std::fmt::Display;
use std::str::FromStr;
use std::fs;
use std::{thread, time::Duration};
use sysfs_gpio::{Direction, Pin};

#[derive(Serialize, Deserialize, Debug)]
struct APIResponse {
    kind: String,
    etag: String,
    #[serde(rename = "pageInfo")]
    page_info: PageInfo,
    items: Vec<Item>,
}

#[derive(Serialize, Deserialize, Debug)]
struct PageInfo {
    #[serde(rename = "totalResults")]
    total_results: i16,
    #[serde(rename = "resultsPerPage")]
    results_per_page: i16,
}

#[derive(Serialize, Deserialize, Debug)]
struct Item {
    kind: String,
    etag: String,
    id: String,
    statistics: Statistics,
}

#[derive(Copy, Clone, Serialize, Deserialize, Debug)]
struct Statistics {
    #[serde(rename = "viewCount", deserialize_with = "from_str")]
    view_count: i16,
    #[serde(rename = "subscriberCount", deserialize_with = "from_str")]
    subscriber_count: i16,
    #[serde(rename = "hiddenSubscriberCount")]
    hidden_subscriber_count: bool,
    #[serde(rename = "videoCount", deserialize_with = "from_str")]
    video_count: i16,
}

fn from_str<'de, T, D>(deserializer: D) -> Result<T, D::Error>
    where T: FromStr,
          T::Err: Display,
          D: Deserializer<'de>
{
    let s = String::deserialize(deserializer)?;
    T::from_str(&s).map_err(de::Error::custom)
}

#[tokio::main]
async fn main() {
    // send one test gate at startup
    send_gate(199);

    let mut response = get_channel_statistics("UCED6_kloOBTcWguoq5vKpjQ")
        .await
        .expect("Error while fetching API");
    let mut statistics: Statistics = parse_response(response).await;
    let mut old_sub_count = statistics.subscriber_count;
    let mut sub_count: i16;
    loop {
        response = get_channel_statistics("UCED6_kloOBTcWguoq5vKpjQ")
            .await
            .expect("Error while fetching API");
        statistics = parse_response(response).await;
        sub_count = statistics.subscriber_count;
        if sub_count > old_sub_count {
            old_sub_count = sub_count;
            println!("New Subscriber!!");
            send_gate(199);
        }
        println!("Subscriber count: {}", sub_count);
        thread::sleep(Duration::from_millis(5000));
    }
}

fn send_gate(pin: u64) {
    let output_pin = Pin::new(pin);
    output_pin.with_exported(|| {
        output_pin.set_direction(Direction::Out).unwrap();
        output_pin.set_value(1).unwrap();
        thread::sleep(Duration::from_millis(20));
        output_pin.set_value(0).unwrap();
        Ok(())
    });
}

async fn parse_response(response: reqwest::Response) -> Statistics {
    let api_response = match response.status() {
        reqwest::StatusCode::OK => response.json::<APIResponse>()
            .await
            .expect("Couldn't serialize json"),
        other => {
            panic!("Unexpected: {:?}", other);
        }
    };
    let statistics = api_response.items.first().unwrap().statistics;
    statistics
}

async fn get_channel_statistics(channel_id: &str) -> Result<reqwest::Response, reqwest::Error> {
    let api_key = fs::read_to_string("api_key.txt")
        .expect("Couldn't read api_key.txt");

    let client = reqwest::Client::new();
    let response = client.get("https://www.googleapis.com/youtube/v3/channels")
        .query(&[("key", api_key.as_str()), ("id", channel_id), ("part", "statistics")])
        .send()
        .await;
    response
}
